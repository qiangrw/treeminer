class TripleNode
  attr_accessor :tid, :m, :scope
  def initialize(tid, m, scope) 
    @tid = tid
    @m = m
    @scope = scope
  end

  def scope_in node
    return false if @scope[0] == node.scope[0] and @scope[1] == node.scope[1]
    @scope[0] >= node.scope[0] and @scope[1] <= node.scope[1]
  end

  def scope_strict_less node
    @scope[1] < node.scope[0]
  end

  def display
    puts "    #{@tid},#{@m},[#{@scope.join ","}]"
  end
end

class EqNode
  attr_accessor :item, :parent_node, :support, :triple_node_list, :triple_node_hash
  def initialize(item, parent_node, support, triple_node_list=nil)
    @item = item
    @parent_node = parent_node
    @support = support
    @triple_node_list = triple_node_list
    @triple_node_hash = Hash.new()
    if triple_node_list.nil? then 
      @triple_node_list = Array.new()
    else
      # Update triple node hash
      start_index = 0
      @triple_node_list.each do |triple_node|
        # First time the tid appear
        unless @triple_node_hash.has_key? triple_node.tid
          @triple_node_hash[triple_node.tid] = start_index
        end
        start_index += 1
      end
    end
  end

  def add_triple triple_node
    unless @triple_node_hash.has_key? triple_node.tid
      @triple_node_hash[triple_node.tid] = @triple_node_list.size
      @support += 1
    end
    @triple_node_list.push triple_node
  end

  def update_support
    @support = 0
    tid_hash = Array.new()
    @triple_node_list.each do |triple|
      if(tid_hash[triple.tid].nil? || tid_hash[triple.tid] == 0) 
        @support += 1
        tid_hash[triple.tid] = 1
      end
    end
  end

  def join node_y, prefix=nil
    if @parent_node == node_y.parent_node
      # Case I
      result = Array.new
      unless prefix.nil? 
        #puts "add (y,j)"
        # I.a add(y,j) and (y,j_ni) to Px
        # Add (y,j)
        node1 = EqNode.new(node_y.item, parent_node, 0, nil)
        # Out-Scope Test
        @triple_node_list.each do |lx|
          tid_start =  node_y.triple_node_hash[lx.tid]
          next if tid_start.nil?
          begin
            ly = node_y.triple_node_list[tid_start]
            break if ly.tid != lx.tid
            tid_start += 1
            # Satisfy three conditions
            if ly.tid == lx.tid and ly.m == lx.m and lx.scope_strict_less ly
              # Both x and y are descendants of some node at position j
              extend_m = Array.new(ly.m) 
              extend_m.push lx.scope[0]
              node1.add_triple TripleNode.new(ly.tid, extend_m, ly.scope)
            end
          end while tid_start < node_y.triple_node_list.size
        end
        #node1.update_support
        result.push node1
      end
      #puts "add (y,j+1)"
      # I.b add(y,j+1) to P_x
      node = EqNode.new(node_y.item, parent_node+1, 0, nil)
      # In-Scope Test
      @triple_node_list.each do |lx|
        #node_y.triple_node_list.each do |ly|
        tid_start =  node_y.triple_node_hash[lx.tid]
        next if tid_start.nil?
        begin
          ly = node_y.triple_node_list[tid_start]
          break if ly.tid != lx.tid
          tid_start += 1
          # Satisfy three conditions
          if ly.tid == lx.tid and ly.m == lx.m and ly.scope_in lx
            # y is a descendant of x in some input tree tid
            # extend the match label my of the old prefix P
            extend_m = Array.new(ly.m) 
            extend_m.push lx.scope[0]
            node.add_triple TripleNode.new(ly.tid, extend_m, ly.scope)
          end
        end while tid_start < node_y.triple_node_list.size
      end
      #node.update_support
      result.push node
      return result
    else @parent_node > node_y.parent_node
      #puts "case 2 add (y,j)"
      # Case 2 Add(y,j) to class P_x_i
      node1 = EqNode.new(node_y.item, parent_node, 0, nil)
      # Out-Scope Test
      @triple_node_list.each do |lx|
        #node_y.triple_node_list.each do |ly|
        tid_start =  node_y.triple_node_hash[lx.tid]
        next if tid_start.nil?
        begin
          ly = node_y.triple_node_list[tid_start]
          break if ly.tid != lx.tid
          tid_start += 1
          # Satisfy three conditions
          if ly.tid == lx.tid and ly.m == lx.m and lx.scope_strict_less ly
            # Both x and y are descendants of some node at position j
            extend_m = Array.new(ly.m) 
            extend_m.push lx.scope[0]
            node1.add_triple TripleNode.new(ly.tid, extend_m, ly.scope)
          end
        end while tid_start < node_y.triple_node_list.size
      end
      #node1.update_support
      return [node1]
    end
    # else Case 3 - No new candidate is possible
    return
  end
  
  def display
    puts "  ELEMENT(#{@item},#{@parent_node})"
    puts "  SUPPORT: #{@support}"
    puts "  TripleList:"
    @triple_node_list.each do |triple|
      triple.display
    end
  end
end


