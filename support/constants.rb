class Constants
  BACKTRACK_FLAG = -1    # back track flag for tree's string encode
  NO_NODE = -1           # link to the root node
  MAX_FSET_COUNT = 2000  # the max fset can be computed in the forest
end
