class Tree
  attr_reader :tid, :max_label, :item_scope_list, :item_list

  def initialize(string_encode, tid=0)
    @string_encode = string_encode.strip
    @max_label = 0
    @tid = tid
    @item_scope_list = []
    @item_list = []
    @vertical_formats = self.compute_scope_list(string_encode)
    #p @vertical_formats
  end

  # Compute every node's scope list
  def compute_scope_list(string_encode)
    vertical_array = Array.new()
    index = 0
    node_stack = []
    string_encode.split.each do |element|
      element = element.to_i
      if(element == -1)
        # Trace back, Pop out current node
        child_node = node_stack.pop
        # Top element in the stack is the parent node
        parent_node = node_stack[-1]
        # Update the right most scope list value
        vertical_array[parent_node][1][1] = vertical_array[child_node][1][1] unless parent_node.nil? 
      else
        # A real node, first meet
        vertical_array[index] = [element,[index,index]]
        if element > @max_label then @max_label = element end
        node_stack.push index
        index += 1
      end
    end

    # Assign the scope_list hash table for furthur using
    vertical_array.each do |element_scope|
      element, scope = element_scope[0], element_scope[1]
      # A fresh-new item comes
      if @item_scope_list[element].nil?
        @item_scope_list[element] = Array.new
        @item_list.push element
      end
      @item_scope_list[element].push scope
    end
    return vertical_array
  end

end
