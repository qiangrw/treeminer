class EqClass	
  attr_accessor :prefix, :node_list
  def initialize(prefix = nil, node_list = nil)
    @prefix = prefix
    @node_list = node_list.nil? ? Array.new() : node_list
  end

  def add_node node
    @node_list.push node
  end

  def display
    puts "-PREFIX:{#{prefix.join ","}}"
    puts "-NODE-LIST:"
    @node_list.each do |node|
      node.display
    end
  end
end
