require File.expand_path "../support/tree", __FILE__
require File.expand_path "../support/eqnode", __FILE__
require File.expand_path "../support/eqclass", __FILE__
require File.expand_path "../support/constants", __FILE__

# The function to generate item's scope list from the forest 
# This scope list is used to initiate the EqNode in EqClass
def get_scope_list(forest, item)
  triple_node_list = Array.new
  forest.each do |tree|
    next if tree.item_scope_list[item].nil? # Not exists in this tree
    tree.item_scope_list[item].each do |scope|
      triple_node_list.push TripleNode.new(tree.tid, [], scope)
    end
  end
  return triple_node_list
end          

# The core function to enumerate frequent subtree
def enumerate_frequent_subtree p, it, minsup, f_set, debug = false
  p.node_list.each do |node_x|
    p_x_i = EqClass.new()
    new_prefix = Array.new(p.prefix)

    # PUSH THE Constants::BACKTRACK_FLAG TO THE PREFIX
    trace_count,cur_scope = 0,0
    parent_node = node_x.parent_node
    p.prefix.size.times do |i|
      if(p.prefix[i] ==  Constants::BACKTRACK_FLAG)
        trace_count -= 1
      else
        trace_count += 1
      end
      cur_scope = trace_count if i == parent_node
    end
    #puts "cur_scope:#{cur_scope} trace_count:#{trace_count}"
    while(trace_count > cur_scope) do
      new_prefix.push Constants::BACKTRACK_FLAG
      trace_count -= 1
    end

    new_prefix.push node_x.item
    p_x_i.prefix = new_prefix
    #p_x_i.prefix = p.prefix.push node_x.item
    p.node_list.each do |node_y|
      r_set = node_x.join node_y,p.prefix 
      next if r_set.nil? || r_set.size <= 0
      r_set.each do |r_node|
        if r_node.support >= minsup then
          p_x_i.add_node r_node
        end
      end
    end
    if p_x_i.node_list.size > 0
      puts "------------------------F#{it}------------------------------" if debug
      p_x_i.display if debug #if it > 2
      f_set[it].push p_x_i
      enumerate_frequent_subtree p_x_i, it+1, minsup, f_set, debug
    end
  end
end


minsup_per = 1.0
debug = false
db_file = "data/test.dat"

if ARGV.length < 2
  puts "TreeMiner"
  puts "  Version:1.0"
  puts "  Author: Runwei Qiang <qiangrw@gmail.com>"
  puts "  Licence: Free for academic use."
  puts "-----------------------------------------"
  puts "USUAGE: ruby #{$0} infile_path percentage_support display_frequent_sets(default 0)"
  exit
end
db_file, minsup_per = ARGV[0], ARGV[1].to_f
debug = true if(!ARGV[2].nil? && ARGV[2].to_i != 0)
unless File.exists? db_file
  puts "#{db_file} not exists"
  exit
end


f_set = Array.new(Constants::MAX_FSET_COUNT){Array.new()}
# Compute F1()
max_item_label,tree_count,tid = 0,0,0
item_count = Array.new()
item_flag = Array.new()
forest = Array.new()

puts "start to load all the trees"
File.open(db_file) {|f|
  f.each_line do |string_encode|
    tree = Tree.new(string_encode,tid)
    forest.push tree
    string_encode.split.each do |item|
      item = item.to_i
      if(item != Constants::BACKTRACK_FLAG)
        # A real node, first meet
        if item > max_item_label then max_item_label = item end
        # Add item support if not added before in this tree
        if(item_flag[item].nil? || item_flag[item] != tid) 
          if item_count[item].nil? then item_flag[item],item_count[item] = 0,0 end
          item_count[item] += 1
          item_flag[item] = tid
        end
      end
    end
    tid += 1
  end
}
puts "load trees done"
tree_count = tid
minsup = minsup_per * tree_count
puts "TOTAL TREE NO: #{tree_count}"
puts "MAX ITEM LABEL: #{max_item_label}"
puts "MIN SUP: #{minsup}"


p1 = EqClass.new()
# Get the f1 count
f1_count = 0
puts "------------------------F1------------------------------" if debug
(1..max_item_label).each do |item|
  if !item_count[item].nil? && item_count[item] >= minsup
    f1_count += 1
    # WITH NODE SORTED ASC
    node = EqNode.new(item, Constants::NO_NODE, item_count[item],get_scope_list(forest,item))
    p1.add_node node
    puts "#{item} - #{item_count[item]}" if debug
  end
end
puts "F1 -- #{f1_count}"

puts "------------------------F2------------------------------" if debug
# Generate P2 with p1
p1.node_list.each do |node_x|
  p_x_i = EqClass.new()
  p_x_i.prefix = [node_x.item]
  p1.node_list.each do |node_y|
    r_set = node_x.join node_y,p1.prefix 
    next if r_set.nil? || r_set.size <=  0
    r_set.each do |r_node|
      if r_node.support >= minsup then
        p_x_i.add_node r_node
      end
    end
  end
  if p_x_i.node_list.size > 0
    p_x_i.display if debug
    f_set[2].push p_x_i
    enumerate_frequent_subtree p_x_i, 3, minsup, f_set, debug
  end
end

(2..Constants::MAX_FSET_COUNT).each do |i|
  break if f_set[i].nil? || f_set[i].size == 0
  count = 0
  f_set[i].each do |p|
    count += p.node_list.size
  end
  puts "F#{i} -- #{count}"
end
