#!/usr/bin/ruby 
# This tool is used to change the input file format
# Change to adapt to the vtreeminer's input format, such as
# id id length string_encoding
# example: 
# 0 0 7 1 2 -1 3 4 -1 -1
# 1 1 11 2 1 2 -1 4 -1 -1 2 -1 3 -1
# 2 2 15 1 3 2 -1 -1 5 1 2 -1 3 4 -1 -1 -1 -1

if ARGV.length < 2
  puts "USUAGE: #{$0} inputfile outputfile"
  exit
end

input,output = ARGV
tid = 0
File.open(output,"w") { |out|
  File.open(input) { |f|
    f.each_line do |line|
      length = line.strip.split.size
      out.puts "#{tid} #{tid} #{length} #{line.strip}"
      tid += 1
    end
  }
}
